﻿using System;

namespace PalfysKorszak
{
    class CserelgetosJatek
    {
        static void Run()
        {
            bool[,] tábla = Inicializálás();
            Állapot(tábla);
            while (!VégeVanE(tábla))
            {
                Akció(tábla);
                Állapot(tábla);
            }
            Console.WriteLine("Gratulálok, győztél!");
            Console.ReadLine();
        }
        static bool VégeVanE(bool[,] tábla)
        {
            for (int i = 0; i < tábla.GetLength(0); i++)
                for (int j = 0; j < tábla.GetLength(1); j++)
                    if (tábla[i, j] == false)
                        return false;
            return true;
        }
        static void Akció(bool[,] tábla)
        {
            bool bemenetjo = false;
            int sor = int.MaxValue, oszlop = int.MaxValue;
            while (bemenetjo == false)
            {
                try
                {
                    Console.WriteLine("Add meg a sort és az oszlopot szóközzel elválasztva!");
                    string[] bemenet = Console.ReadLine().Split(' ');
                    bemenetjo = true;
                    sor = int.Parse(bemenet[0]) - 1;
                    oszlop = int.Parse(bemenet[1]) - 1;
                }
                catch (Exception) { }
            }
            if (sor >= 0 && sor < tábla.GetLength(0) && oszlop >= 0 && oszlop < tábla.GetLength(1))
                tábla[sor, oszlop] = !tábla[sor, oszlop];
            if (sor >= 1 && sor < tábla.GetLength(0) && oszlop >= 0 && oszlop < tábla.GetLength(1))
                tábla[sor - 1, oszlop] = !tábla[sor - 1, oszlop];
            if (sor >= 0 && sor < tábla.GetLength(0) - 1 && oszlop >= 0 && oszlop < tábla.GetLength(1))
                tábla[sor + 1, oszlop] = !tábla[sor + 1, oszlop];
            if (sor >= 0 && sor < tábla.GetLength(0) && oszlop >= 1 && oszlop < tábla.GetLength(1) + 1)
                tábla[sor, oszlop - 1] = !tábla[sor, oszlop - 1];
            if (sor >= 0 && sor < tábla.GetLength(0) && oszlop >= 0 && oszlop < tábla.GetLength(1) - 1)
                tábla[sor, oszlop + 1] = !tábla[sor, oszlop + 1];
        }
        static bool[,] Inicializálás()
        {
            Console.ForegroundColor = ConsoleColor.White;
            bool bemenetjo = false;
            int beszam = 0;
            while (bemenetjo == false)
            {
                try
                {
                    Console.Clear();
                    Console.WriteLine("Add meg, hogy hány sor/oszlop legyen (min. 2, max. 9)!");
                    Console.WriteLine("Ha párosat adsz meg, könnyebb lesz.\nHa páratlant, akkor nehezebb.");
                    beszam = int.Parse(Console.ReadLine());
                    if (beszam > 1 && beszam < 10)
                        bemenetjo = true;
                }
                catch (Exception) { }
            }
            bool[,] tábla = new bool[beszam, beszam];
            if (beszam % 2 == 1) //ha páratlan számot ad meg
            {
                for (int i = 1; i < beszam - 1; i++)
                    tábla[i, tábla.GetLength(0) / 2] = true;
                for (int i = 1; i < beszam - 1; i++)
                    tábla[tábla.GetLength(0) / 2, i] = true;
            }
            return tábla;
        }
        static void Állapot(bool[,] tábla)
        {
            Console.Clear();
            Console.Write("   ");
            for (int i = 1; i <= tábla.GetLength(0); i++)
                Console.Write("{0} ", i);
            Console.WriteLine("\n");
            for (int i = 0; i < tábla.GetLength(0); i++)
            {
                Console.Write("{0}  ", i + 1);
                for (int j = 0; j < tábla.GetLength(1); j++)
                {
                    if (tábla[i, j] == true)
                        Console.Write("+ ");
                    else
                        Console.Write("- ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("\n");
        }
    }
}