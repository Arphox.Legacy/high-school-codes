﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace PalfysKorszak
{
    class TabellaGeneralo
    {
        static void Run()
        {
            Console.ForegroundColor = ConsoleColor.White;
            //Jelentkező játékosok listájának beolvasása és azok számának megállapítása
            string[] jatekosoklistaja = File.ReadAllLines(@"e:\source\jelentkezok.txt", Encoding.Default);
            int jatekosokszama = jatekosoklistaja.Length;

            //Keretszámok kiszámítása
            List<int> keretszamok = new List<int>();
            for (int i = 4; i <= 100000; i *= 2)
                keretszamok.Add(i);

            //Keretszám kiszámítása
            int szamlalo = 0;
            for (szamlalo = 0; keretszamok[szamlalo] < jatekosokszama; szamlalo++)
            { }
            int keretszam = keretszamok[szamlalo];

            //Jelentkezők + Wildcardok felépítése
            string[] jelentkezok = new string[keretszam]; //játékosok + wildcardok
            for (int i = 0; i < jelentkezok.Length; i++)
            {
                if (i < jatekosokszama)
                    jelentkezok[i] = jatekosoklistaja[i];
                else
                    jelentkezok[i] = "Wildcard";
            }

            List<int> voltmar = new List<int>();
            int sor = 1;
            Console.WriteLine("A játékosok száma: {0}\n", jatekosokszama);
            while (voltmar.Count < keretszam)
            {
                Random x1 = new Random();
                int r1 = x1.Next(keretszam);
                while (voltmar.Contains(r1))
                    r1 = x1.Next(keretszam);

                Random x2 = new Random();
                int r2 = x2.Next(keretszam);
                while (voltmar.Contains(r2) || r1 == r2)
                    r2 = x2.Next(keretszam);

                //színes kiíratás
                Console.Write("{0,2} ", sor);
                if (jelentkezok[r1] == "Wildcard")
                    Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("{0,20}", jelentkezok[r1]);
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(" - ");
                if (jelentkezok[r2] == "Wildcard")
                    Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(jelentkezok[r2]);
                Console.ForegroundColor = ConsoleColor.White;

                voltmar.Add(r1);
                voltmar.Add(r2);
                sor++;
            }

            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("\nListázás kész.");
            Console.ReadLine();
        }
    }
}