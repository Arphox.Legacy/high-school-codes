﻿using System;

namespace PalfysKorszak
{
    class NyilasMenu
    {
        static void Run()
        {
            /* A program alap részeit NEM én készítettem, csak módosítottam rajta
             * a könnyebb megértés és a videóban való előadhatóság érdekében.
             * A program elkészítéséért járó elismerés nem az enyém. */

            Console.ForegroundColor = ConsoleColor.White;
            Console.CursorVisible = false;
            string[] menüpontok = new string[5];
            menüpontok[0] = " File ";
            menüpontok[1] = " Edit ";
            menüpontok[2] = " Load ";
            menüpontok[3] = " Save ";
            menüpontok[4] = " Exit ";
            int válasz;
            do
            {
                válasz = menü(menüpontok);
                switch (válasz)
                {
                    case 0:
                        Console.Clear();
                        Console.Write("File");
                        Console.ReadLine();
                        break;
                    case 1:
                        Console.Clear();
                        Console.Write("Edit");
                        Console.ReadLine();
                        break;
                    case 2:
                        Console.Clear();
                        Console.Write("Load");
                        Console.ReadLine();
                        break;
                    case 3:
                        Console.Clear();
                        Console.Write("Save");
                        Console.ReadLine();
                        break;
                }
            } while (válasz != 4);
        }
        static int menü(string[] menüpontok)
        {
            int aktuálismenüpont = 0;
            ConsoleKeyInfo gomb;
            do
            {
                for (int i = 0; i < menüpontok.Length; i++) //ez a rész kiírja a menüpontokat
                {
                    Console.SetCursorPosition(0, i);
                    if (aktuálismenüpont == i)
                        Console.BackgroundColor = ConsoleColor.Blue;
                    else
                        Console.BackgroundColor = ConsoleColor.Black;
                    Console.Write(menüpontok[i]);
                }
                gomb = Console.ReadKey(true);
                if (gomb.Key == ConsoleKey.DownArrow)
                    aktuálismenüpont++;
                else if (gomb.Key == ConsoleKey.UpArrow)
                    aktuálismenüpont--;
                else if (gomb.Key == ConsoleKey.Enter)
                    return aktuálismenüpont;

                if (aktuálismenüpont < 0) //ha felfelé "kifutnánk"
                    aktuálismenüpont = menüpontok.Length - 1;
                else if (aktuálismenüpont > menüpontok.Length - 1) //ha lefelé "kifutnánk"
                    aktuálismenüpont = 0;
            } while (gomb.Key != ConsoleKey.Enter);
            return -1;
        }
    }
}