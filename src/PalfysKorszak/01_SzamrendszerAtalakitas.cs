﻿using System;

namespace PalfysKorszak
{
    class SzamrendszerAtalakitas
    {
        static void Run()
        {
            Console.WriteLine("Adja meg a számot!");
            int a = int.Parse(Console.ReadLine());

            Console.WriteLine("Szám\tBináris\tHexadecimális");
            for (int i = 1; i <= a; i++)
            {
                Console.WriteLine("{0}\t{1}\t{2}", i, Convert.ToString(i, 2), Convert.ToString(i, 16));
            }
            Console.ReadLine();
        }
    }
}