﻿using System;

namespace PalfysKorszak
{
    class _07_Szamkitalalos
    {
        static void Run()
        {
            bool ki = false;
            while (!ki)
            {
                Console.Clear();
                char v = ' ';
                Random r = new Random();
                int jatekostipp = 0, szam = 0;
                do
                {
                    Console.WriteLine("Válassz játékmódot!\n1. Te gondolsz egy számra (1-100)\n2. A gép gondol egy számra (1-100)");
                    v = Console.ReadKey(true).KeyChar;
                    Console.Clear();
                }
                while (v != '1' && v != '2');
                szam = r.Next(1, 100);
                if (v == '1') //játékos gondol
                {
                    bool nyerte = false;
                    for (int i = 0; i < 5; ++i)
                    {
                        while (true)
                        {
                            Console.Write("Mi a tipped? ");
                            try { jatekostipp = int.Parse(Console.ReadLine()); break; }
                            catch { }
                        }
                        if (szam < jatekostipp)
                            Console.WriteLine("Ennél a szám kisebb!");
                        else if (szam > jatekostipp)
                            Console.WriteLine("Ennél a szám nagyobb!");
                        else
                        {
                            Console.WriteLine("Nyertél!"); nyerte = true; break;
                        }
                    }
                    if (nyerte == false) { Console.WriteLine("Vesztettél, a szám {0} volt.", szam); }
                }
                else //gép gondol
                {
                    int geptipp = 50, min = 0, max = 100;
                    bool nyerte = false;
                    Console.WriteLine("Gondolj egy számra!");
                    for (int i = 0; i < 5; ++i)
                    {
                        Console.WriteLine("A gép tippje: {0}. A szám ennél? (k/n/e)", geptipp);
                        do
                        {
                            v = Console.ReadKey(true).KeyChar;
                        }
                        while (v != 'k' && v != 'n' && v != 'e');
                        switch (v)
                        {
                            case 'k':
                                {
                                    if (i == 4) { geptipp = r.Next(min, max); }
                                    max = geptipp;
                                    geptipp -= (max - min) / 2;
                                    break;
                                }
                            case 'n':
                                {
                                    if (i == 4) { geptipp = r.Next(min, max); }
                                    min = geptipp;
                                    geptipp += (max - min) / 2;
                                    break;
                                }
                            case 'e':
                                {
                                    Console.WriteLine("A gép nyert!");
                                    nyerte = true;
                                    break;
                                }
                        }
                    }
                    if (nyerte == false) { Console.WriteLine("Te nyertél!"); }
                }
                Console.WriteLine("\nAkarsz még játszani? (i/n)");
                do
                {
                    v = Console.ReadKey(true).KeyChar;
                }
                while (v != 'i' && v != 'n');
                if (v == 'n') { ki = true; }
            }
        }

    }
}
