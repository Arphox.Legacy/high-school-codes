﻿using System;
using System.Collections;

namespace PalfysKorszak
{
    class _04_Komplex_Auto
    {
        static ArrayList tipus = new ArrayList();
        static ArrayList evjarat = new ArrayList(); //gyártási év
        static ArrayList rendszam = new ArrayList();
        static ArrayList ar = new ArrayList();
        static ArrayList eladva = new ArrayList();

        static void Run()
        {
            Demo(); //Ha nem akarsz példaadatbázist, kommenteld be
            Menu();
        }
        static void Menu()
        {
            do //menü végtelen ismétlés
            {
                Console.Clear();
                Console.WriteLine("Menü");
                Console.WriteLine("\t1. Adatbevitel");
                Console.WriteLine("\t2. Adatbázis listázása");
                Console.WriteLine("\t3. Legöregebb autó megkeresése");
                Console.WriteLine("\t4. Legfiatalabb autó megkeresése");
                Console.WriteLine("\t5. Eladott, eladatlan autók száma");
                Console.WriteLine("\t6. Keresés rendszám alapján");
                Console.WriteLine("\t7. Keresés típus alapján");
                Console.WriteLine("\t0. Kilépés");
                Console.Write("\nVálassz a fentiek közül (a menüpont számát): ");
                string valasz = Console.ReadLine();

                switch (valasz)
                {
                    case "1":
                        Adatbevitel_1();
                        break;
                    case "2":
                        Listazas_2();
                        break;
                    case "3":
                        Legoregebb_3();
                        break;
                    case "4":
                        Legfiatalabb_4();
                        break;
                    case "5":
                        Eladasi_5();
                        break;
                    case "6":
                        KeresRendszam_6();
                        break;
                    case "7":
                        KeresTipus_7();
                        break;
                    case "8":
                        break;
                    case "0":
                        Environment.Exit(-1);
                        break;
                }
            }
            while (true);
        }
        static void Adatbevitel_1()
        {
            string valasz;
            do
            {
                Console.Clear();
                Console.WriteLine("1. Adatbevitel\n========================================================\n");

                Be("Add meg a jármű típusát!", out valasz);
                tipus.Add(valasz);
                Be("Add meg a jármű rendszámát!", out valasz);
                rendszam.Add(valasz);
                Be("Add meg a jármű gyártási évét!(pl.: 1999)", out valasz);
                evjarat.Add(valasz);
                Be("Add meg a jármű árát!(pl.: 1000000)", out valasz);
                ar.Add(valasz);
                Be("El van adva? (igen/nem)", out valasz);
                eladva.Add(valasz);
                Console.WriteLine("\nAkar még adatot bevinni? (igen/nem)");
                valasz = Console.ReadLine();

                while (valasz != "igen" && valasz != "nem")
                {
                    Console.WriteLine("\nAkar még adatot bevinni? (igen/nem)");
                    valasz = Console.ReadLine();
                }
            }
            while (valasz == "igen");
            System.Threading.Thread.Sleep(1000);
        }
        static void Listazas_2()
        {
            Console.Clear();
            Console.WriteLine("2. Adatbázis listázása\n========================================================\n");
            Console.WriteLine("Típus\t  Rendszám  Évjárat\tÁr\tEladva\n========================================================\n");

            for (int i = 0; i < tipus.Count; i++)
            {
                if (i % 2 == 0)
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("{0}\t  {1}    {2}     {3}\t {4}\n", tipus[i], rendszam[i], evjarat[i], ar[i], eladva[i]);
                    Console.ResetColor();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write("{0}\t  {1}    {2}     {3}\t {4}\n", tipus[i], rendszam[i], evjarat[i], ar[i], eladva[i]);
                    Console.ResetColor();
                }
            }

            Console.ReadLine();
        }
        static void Legoregebb_3()
        {
            Console.Clear();
            Console.WriteLine("3. Legöregebb autó megkeresése\n========================================================\n");
            Console.WriteLine("Típus\t  Rendszám  Évjárat\tÁr\tEladva\n========================================================\n");

            int min = Convert.ToInt16(evjarat[0]);
            int minindex = 0; //ez a helyes autó indexe

            for (int i = 0; i < evjarat.Count; i++)
            {
                if (min > Convert.ToInt32(evjarat[i]))
                {
                    min = Convert.ToInt32(evjarat[i]);
                    minindex = i; //ha talál egy régebbit, az indexét berakja ide
                }//így a legkisebb indexű fog a végére a minindex-be kerülni
            }

            Console.Write("{0}\t  {1}    {2}     {3}\t {4}\n", tipus[minindex], rendszam[minindex], evjarat[minindex], ar[minindex], eladva[minindex]);
            Console.ReadLine();
        }
        static void Legfiatalabb_4()
        {
            Console.Clear();
            Console.WriteLine("4. Legfiatalabb autó megkeresése\n========================================================\n");

            int max = Convert.ToInt32(evjarat[0]);
            int maxindex = 0; //ez a helyes autó indexe

            for (int i = 0; i < tipus.Count; i++)
            {
                if (max < Convert.ToInt32(evjarat[i]))
                {
                    max = Convert.ToInt32(evjarat[i]);
                    maxindex = i; //ha talál egy régebbit, az indexét berakja ide
                }//így a legkisebb indexű fog a végére a maxindex-be kerülni
            }

            Console.WriteLine("Típus\t  Rendszám  Évjárat\tÁr\tEladva\n========================================================\n");
            Console.Write("{0}\t  {1}    {2}     {3}\t {4}\n", tipus[maxindex], rendszam[maxindex], evjarat[maxindex], ar[maxindex], eladva[maxindex]);

            Console.ReadLine();
        }
        static void Eladasi_5()
        {
            Console.Clear();
            Console.WriteLine("5. Eladott, eladatlan autók száma\n========================================================\n");

            int eladott = 0, eladatlan = 0;

            for (int i = 0; i < eladva.Count; i++)
            {
                if ((string)(eladva[i]) == "IGEN") //a (string) kell (másképp warning)
                    eladott++;
                else
                    eladatlan++;
            }

            Console.WriteLine("Eladott autók száma: " + eladott);
            Console.WriteLine("Eladatlan autók száma: " + eladatlan);
            Console.ReadLine();
        }
        static void KeresRendszam_6()
        {
            string valasz = "";
            do
            {
                Console.Clear();
                Console.WriteLine("6. Keresés rendszám alapján\n========================================================\n");
                Console.Write("Adja meg a keresendő rendszámot: ");

                bool van = false;
                string keres = Console.ReadLine();
                keres = keres.ToUpper(); //nagybetűkkel dolgozunk
                Console.WriteLine("\n\nTípus\t  Rendszám  Évjárat\tÁr\tEladva\n========================================================\n");
                for (int i = 0; i < evjarat.Count; i++)
                {
                    if ((string)rendszam[i] == keres)
                    {
                        Console.Write("{0}\t  {1}    {2}     {3}\t {4}\n", tipus[i], rendszam[i], evjarat[i], ar[i], eladva[i]);
                        van = true;
                        break;
                    }
                }

                if (!van)
                    Console.WriteLine("\nNincs ilyen autó az adatbázisban.\n");
                else
                    Console.WriteLine();

                Console.WriteLine("Akar még keresni? (igen/nem)");
                valasz = Console.ReadLine();
                while (valasz != "igen" && valasz != "nem")
                {
                    Console.WriteLine("Akar még keresni? (igen/nem)");
                    valasz = Console.ReadLine();
                }

            }
            while (valasz == "igen");
            System.Threading.Thread.Sleep(1000);
        }
        static void KeresTipus_7()
        {
            string valasz = "";
            do
            {
                Console.Clear();
                Console.WriteLine("7. Keresés típus alapján\n========================================================\n");
                Console.Write("Adja meg a keresendő típust: ");
                string keres = Console.ReadLine();
                bool van = false;
                keres = keres.ToUpper(); //nagybetűkkel dolgozunk

                Console.WriteLine("\n\nTípus\t  Rendszám  Évjárat\tÁr\tEladva\n========================================================\n");
                for (int i = 0; i < tipus.Count; i++)
                {
                    if ((string)tipus[i] == keres)
                    {
                        Console.Write("{0}\t  {1}    {2}     {3}\t {4}\n", tipus[i], rendszam[i], evjarat[i], ar[i], eladva[i]);
                        van = true;
                    }
                }

                if (!van)
                    Console.WriteLine("Nincs ilyen típusú autó az adatbázisban.\n");
                else
                    Console.WriteLine(); //a szép kinézet miatt (ne legyen több üres sor egymás után)

                Console.WriteLine("Akar még keresni? (igen/nem)");
                valasz = Console.ReadLine();
                while (valasz != "igen" && valasz != "nem")
                {
                    Console.WriteLine("Akar még keresni? (igen/nem)");
                    valasz = Console.ReadLine();
                }

            }
            while (valasz == "igen");
            System.Threading.Thread.Sleep(1000);
        }
        static void Be(string szoveg, out string valasz)
        {
            valasz = "";
            Console.WriteLine(szoveg);
            valasz = Console.ReadLine();
            while (valasz == "")
            {
                Console.WriteLine("Nem adott meg adatot, újra!");
                valasz = Console.ReadLine();
            }
            valasz = valasz.ToUpper();
        }
        static void Demo()
        {
            tipus.Add("BMW"); evjarat.Add("2010"); rendszam.Add("LWT-218"); ar.Add("8999000"); eladva.Add("NEM");
            tipus.Add("MAZDA"); evjarat.Add("1990"); rendszam.Add("ASE-342"); ar.Add("5200000"); eladva.Add("IGEN");
            tipus.Add("TOYOTA"); evjarat.Add("2002"); rendszam.Add("KHN-854"); ar.Add("4600000"); eladva.Add("NEM");
            tipus.Add("NISSAN"); evjarat.Add("2008"); rendszam.Add("NKX-333"); ar.Add("6000000"); eladva.Add("NEM");
            tipus.Add("OPEL"); evjarat.Add("2002"); rendszam.Add("HXN-023"); ar.Add("3000000"); eladva.Add("IGEN");
            tipus.Add("OPEL"); evjarat.Add("1998"); rendszam.Add("FND-367"); ar.Add("2600000"); eladva.Add("NEM");
            tipus.Add("BMW"); evjarat.Add("1985"); rendszam.Add("ASD-921"); ar.Add("1800000"); eladva.Add("IGEN");
            tipus.Add("SUZUKI"); evjarat.Add("2006"); rendszam.Add("KLP-288"); ar.Add("2500000"); eladva.Add("IGEN");
        }
   
}
}
