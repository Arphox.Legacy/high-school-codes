﻿using System;

namespace PalfysKorszak
{
    class MasodfokuEgyenletmegoldo
    {
        static void Run()
        {
            Console.Write("Adja meg a másodfokú egyenlet együtthatóit!\na = ");
            double a = double.Parse(Console.ReadLine());
            Console.Write("b = ");
            double b = double.Parse(Console.ReadLine());
            Console.Write("c = ");
            double c = double.Parse(Console.ReadLine());

            double D = (b * b - 4 * a * c);
            if (a == 0)
            {
                Console.WriteLine("Egy valós megoldás: {0}", -c / b);
            }
            else if (b == 0)
            {
                Console.WriteLine("Két valós megoldás:\n{0}\n{1}", Math.Sqrt((-c / a)), -1 * Math.Sqrt(-c / a));
            }
            else
            {
                if (D > 0)
                {
                    double x1 = (-b + System.Math.Sqrt(D)) / (2 * a);
                    double x2 = (-b - System.Math.Sqrt(D)) / (2 * a);
                    Console.WriteLine("Két valós megoldás:\n{0}\n{1}", x1, x2);
                }
                else if (D < 0)
                {
                    D = -D;
                    double x = -b / (2 * a);
                    double j = Math.Sqrt(D) / (2 * a);
                    Console.WriteLine("Két komplex megoldás:\n{0} + {1}j\n{0} - {1}j", x, j);
                }
                else
                {
                    double x = (-b + System.Math.Sqrt(D)) / (2 * a);
                    Console.WriteLine("Egy valós megoldás: {0}", x);
                }
            }
            Console.ReadLine();
        }
    }
}