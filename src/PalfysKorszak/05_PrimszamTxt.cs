﻿using System;
using System.Diagnostics;
using System.IO;

namespace PalfysKorszak
{
    class PrimszamTxt
    {
        static void Run()
        {
            Console.ForegroundColor = ConsoleColor.White;
            int a = 0, i = 0;
            StreamWriter sw = new StreamWriter("primek.txt");
            Console.WriteLine("Az első hány prímszámot szeretné lementeni?");
            int be = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Fájlírás folyamatban...");
            sw.WriteLine("Az első {0} prímszám:", be); sw.WriteLine();
            Stopwatch timer = new Stopwatch();
            timer.Start();

            while (i < be)
            {
                a++;
                if (prim(a))
                {
                    sw.WriteLine(a);
                    i++;
                }
            }
            timer.Stop();
            Console.WriteLine("Fájlírás kész.\n\nFutási idő: {0} ms", timer.ElapsedMilliseconds);

            FileStream datafs = new FileStream("data.txt", FileMode.Append);
            StreamWriter datasw = new StreamWriter(datafs);
            datasw.WriteLine("Prímszámok: {0}\tFutási idő: {1} ms", be, timer.ElapsedMilliseconds);
            datasw.Close();

            sw.Close();
            Console.ReadLine();
        }
        static bool prim(int szam)
        {
            if (szam < 2)
                return false;
            for (int i = 2; i < szam; i++)
            {
                if (szam % i == 0)
                    return false;
                if (i > Math.Sqrt(szam))
                    return true;
            }
            return true;
        }
    }
}