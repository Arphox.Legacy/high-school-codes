﻿using System;
using System.Diagnostics;
using System.IO;

namespace PalfysKorszak
{
    class Bortonsziget
    {
        static void Run()
        {
            int tesztszam = 0;
            #region Bekérések
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Add meg a tesztszámot!");
                try
                {
                    tesztszam = Convert.ToInt32(Console.ReadLine());
                    if (tesztszam > 0 && tesztszam < Math.Pow(2, 32))
                        break;
                }
                catch { }
            }
            float[] statisztika = new float[tesztszam];
            string valasz = " ";
            while (valasz != "a" && valasz != "b" && valasz != "c")
            {
                Console.Clear();
                Console.WriteLine("Minden: a\nNem kell log: b\nCsak statisztika: c");
                valasz = Console.ReadLine();
            }
            #endregion
            Stopwatch stopper = new Stopwatch();
            stopper.Start();
            StreamWriter stat = new StreamWriter("statisztika.txt");
            switch (valasz)
            {
                case "a":
                    {
                        #region Minden
                        for (int teszt = 0; teszt < tesztszam; ++teszt) //tesztek számlálója
                        {
                            Console.Clear();
                            StreamWriter sw = new StreamWriter("log" + teszt + ".txt");
                            int[] rabok = new int[100];
                            bool[] rabokvolt = new bool[100];
                            Random r = new Random();
                            int setaszamlalo = 1, szabadulas = 0;
                            bool lampa = false; //false = kikapcsolva, true = bekapcsolva

                            for (int i = 0; i < 100; ++i) //rabok feltöltése
                            {
                                rabok[i] = i + 1;
                                rabokvolt[i] = false;
                            }

                            /* A kiválaszott rab, a nulladik. CSAK ő kapcsolhatja be a lámpát
                             * Ha kikapcsolva találja a lámpát, akkor bekapcsolja, és a számlálót lépteti
                             * Ha bekapcsolva találja a lámpát, akkor úgy hagyja.
                             */

                            while (szabadulas < 100)
                            {
                                int aktualisrab = r.Next(0, 100);
                                Console.Write("{0:0000}. séta: {1:00}. rab. ", setaszamlalo, aktualisrab);
                                sw.Write("{0:0000}. séta: {1:00}. rab. ", setaszamlalo, aktualisrab);

                                if (lampa)
                                {
                                    if (aktualisrab == 0)
                                    {
                                        Console.Write("KIV, LÁMPA: BE, úgy hagyja.");
                                        sw.Write("KIV, LÁMPA: BE, úgy hagyja.");
                                    }
                                    else if (rabokvolt[aktualisrab] == false) //Ha a lámpa be van kapcsolva, és nem a kiválasztott rab van soron
                                    {
                                        lampa = false; //akkor kikapcsolja
                                        rabokvolt[aktualisrab] = true;
                                        Console.Write("LÁMPA: BE, kikapcsolja.");
                                        sw.Write("LÁMPA: BE, kikapcsolja.");
                                    }
                                    else //Lámpa be, nem a kiválasztott, és ő már nem kapcsolhatja ki
                                    {
                                        Console.Write("LÁMPA: BE, többet nem kapcsolhatja ki, úgy hagyja.");
                                        sw.Write("LÁMPA: BE, többet nem kapcsolhatja ki, úgy hagyja.");
                                    }
                                }
                                else //!lampa
                                {
                                    if (aktualisrab == 0) //ha kikapcsolva találja a lámpát a kiválasztott rab
                                    {
                                        if (szabadulas == 99)
                                        {
                                            ++szabadulas;
                                            Console.Write("Kiszabadultak!");
                                            sw.Write("Kiszabadultak!");
                                        }
                                        else
                                        {
                                            lampa = true; //akkor bekapcsolja
                                            ++szabadulas; //és léptet
                                            Console.Write("KIV, LÁMPA: KI, bekapcsolja, léptet.");
                                            sw.Write("KIV, LÁMPA: KI, bekapcsolja, léptet.");
                                        }
                                    }
                                    else //Ha a lámpa ki van kapcsolva, és nem a kiválasztott rab van soron
                                    {
                                        Console.Write("LÁMPA: KI, úgy hagyja.");
                                        sw.Write("LÁMPA: KI, úgy hagyja.");
                                    }
                                }
                                ++setaszamlalo;
                                Console.WriteLine();
                                sw.WriteLine();
                            }

                            Console.WriteLine("\n\n\nVÉGE!\nEz " + setaszamlalo + " óra = " + setaszamlalo / 24 + " nap, ami napi 12 sétával " + (float)setaszamlalo / 4380 + " évet vett igénybe!");
                            sw.WriteLine();
                            sw.WriteLine("VÉGE!\tEz " + setaszamlalo + " óra = " + setaszamlalo / 24 + " nap, ami napi 12 sétával " + (float)setaszamlalo / 4380 + " évet vett igénybe!");
                            sw.Close();
                            stat.WriteLine("{0:000}. teszt - {1:00000} óra - {2} év", teszt + 1, setaszamlalo, (float)setaszamlalo / 4380);
                            statisztika[teszt] = (float)setaszamlalo / 4380;
                        }
                        #endregion
                        break;
                    }
                case "b":
                    {
                        #region Nincslog
                        for (int teszt = 0; teszt < tesztszam; ++teszt) //tesztek számlálója
                        {
                            Console.Clear();
                            int[] rabok = new int[100];
                            bool[] rabokvolt = new bool[100];
                            Random r = new Random();
                            int setaszamlalo = 1, szabadulas = 0;
                            bool lampa = false; //false = kikapcsolva, true = bekapcsolva

                            for (int i = 0; i < 100; ++i) //rabok feltöltése
                            {
                                rabok[i] = i + 1;
                                rabokvolt[i] = false;
                            }

                            /* A kiválaszott rab, a nulladik. CSAK ő kapcsolhatja be a lámpát
                             * Ha kikapcsolva találja a lámpát, akkor bekapcsolja, és a számlálót lépteti
                             * Ha bekapcsolva találja a lámpát, akkor úgy hagyja.
                             */

                            while (szabadulas < 100)
                            {
                                int aktualisrab = r.Next(0, 100);
                                Console.Write("{0:0000}. séta: {1:00}. rab. ", setaszamlalo, aktualisrab);

                                if (lampa)
                                {
                                    if (aktualisrab == 0)
                                    {
                                        Console.Write("KIV, LÁMPA: BE, úgy hagyja.");
                                    }
                                    else if (rabokvolt[aktualisrab] == false) //Ha a lámpa be van kapcsolva, és nem a kiválasztott rab van soron
                                    {
                                        lampa = false; //akkor kikapcsolja
                                        rabokvolt[aktualisrab] = true;
                                        Console.Write("LÁMPA: BE, kikapcsolja.");
                                    }
                                    else //Lámpa be, nem a kiválasztott, és ő már nem kapcsolhatja ki
                                    {
                                        Console.Write("LÁMPA: BE, többet nem kapcsolhatja ki, úgy hagyja.");
                                    }
                                }
                                else //!lampa
                                {
                                    if (aktualisrab == 0) //ha kikapcsolva találja a lámpát a kiválasztott rab
                                    {
                                        if (szabadulas == 99)
                                        {
                                            ++szabadulas;
                                            Console.Write("Kiszabadultak!");
                                        }
                                        else
                                        {
                                            lampa = true; //akkor bekapcsolja
                                            ++szabadulas; //és léptet
                                            Console.Write("KIV, LÁMPA: KI, bekapcsolja, léptet.");
                                        }
                                    }
                                    else //Ha a lámpa ki van kapcsolva, és nem a kiválasztott rab van soron
                                    {
                                        Console.Write("LÁMPA: KI, úgy hagyja.");
                                    }
                                }
                                ++setaszamlalo;
                                Console.WriteLine();
                            }
                            Console.WriteLine("\n\n\nVÉGE!\nEz " + setaszamlalo + " óra = " + setaszamlalo / 24 + " nap, ami napi 12 sétával " + (float)setaszamlalo / 4380 + " évet vett igénybe!");
                            stat.WriteLine("{0:000}. teszt - {1:00000} óra - {2} év", teszt + 1, setaszamlalo, (float)setaszamlalo / 4380);
                            statisztika[teszt] = (float)setaszamlalo / 4380;
                        }

                        #endregion
                        break;
                    }
                case "c":
                    {
                        #region CsakStat
                        Console.Write("\nTesztelés elindítva, kérlek várj...");
                        for (int teszt = 0; teszt < tesztszam; ++teszt)
                        {
                            int[] rabok = new int[100];
                            bool[] rabokvolt = new bool[100];
                            Random r = new Random();
                            int setaszamlalo = 1, szabadulas = 0;
                            bool lampa = false;
                            for (int i = 0; i < 100; ++i)
                            {
                                rabok[i] = i + 1;
                                rabokvolt[i] = false;
                            }
                            while (szabadulas < 100)
                            {
                                int aktualisrab = r.Next(0, 100);

                                if (lampa)
                                {
                                    if (aktualisrab != 0 && rabokvolt[aktualisrab] == false)
                                    {
                                        lampa = false;
                                        rabokvolt[aktualisrab] = true;
                                    }
                                }
                                else
                                {
                                    if (aktualisrab == 0)
                                    {
                                        if (szabadulas == 99)
                                            ++szabadulas;
                                        else
                                        {
                                            lampa = true;
                                            ++szabadulas;
                                        }
                                    }
                                }
                                ++setaszamlalo;
                            }
                            stat.WriteLine("{0:00000 }. teszt - {1:00000} óra - {2} év", teszt + 1, setaszamlalo, (float)setaszamlalo / 4380);
                            statisztika[teszt] = (float)setaszamlalo / 4380;
                            System.Threading.Thread.Sleep(3); //mert így randomabb lesz
                        }
                        #endregion
                        break;
                    }
            }

            stopper.Stop();
            stat.WriteLine();
            float osszeg = 0;
            for (int x = 0; x < tesztszam; ++x)
                osszeg += statisztika[x];
            stat.WriteLine("Átlagidő: {0} év", osszeg / tesztszam);

            stat.WriteLine();
            stat.WriteLine("A teszt ideje : {0} = {1} ms", stopper.Elapsed, stopper.ElapsedMilliseconds);
            stat.Close();
            Console.WriteLine("\nKész.");
            Console.ReadLine();
            Process.Start("notepad.exe", "statisztika.txt");
        }
    }
}