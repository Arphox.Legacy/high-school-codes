﻿using System;
using System.Threading;

namespace PalfysKorszak.PianoSimulator
{
    class Zenegep_v1
    {
        // C    D   E   F   G   A   B   C
        // dó   ré  mi  fá  szó lá  ti  dó

        static void Run()
        {
            Console.WriteLine("Add meg a hangokat!");
            string hangok = Console.ReadLine();
            int hossz = 300;
            for (int i = 0; i < hangok.Length; i++)
            {
                switch (Convert.ToString(hangok[i]))
                {
                    case "a": C5(hossz); break;
                    case "s": D5(hossz); break;
                    case "d": E5(hossz); break;
                    case "f": F5(hossz); break;
                    case "g": G5(hossz); break;
                    case "h": A5(hossz); break;
                    case "j": B5(hossz); break;
                    case "k": C6(hossz); break;
                    case " ": Thread.Sleep(300); break;
                }
            }

        }

        //HANGOK C4-től C7-ig

        static void C4(int a)
        {
            Console.Beep(261, a);
        }
        static void C4s(int a)
        {
            Console.Beep(277, a);
        }
        static void D4(int a)
        {
            Console.Beep(293, a);
        }
        static void D4s(int a)
        {
            Console.Beep(311, a);
        }
        static void E4(int a)
        {
            Console.Beep(329, a);
        }
        static void F4(int a)
        {
            Console.Beep(349, a);
        }
        static void F4s(int a)
        {
            Console.Beep(369, a);
        }
        static void G4(int a)
        {
            Console.Beep(391, a);
        }
        static void G4s(int a)
        {
            Console.Beep(415, a);
        }
        static void A4(int a)
        {
            Console.Beep(440, a);
        }
        static void A4s(int a)
        {
            Console.Beep(466, a);
        }
        static void B4(int a)
        {
            Console.Beep(493, a);
        }
        static void C5(int a)
        {
            Console.Beep(523, a);
        }
        static void C5s(int a)
        {
            Console.Beep(554, a);
        }
        static void D5(int a)
        {
            Console.Beep(587, a);
        }
        static void D5s(int a)
        {
            Console.Beep(622, a);
        }
        static void E5(int a)
        {
            Console.Beep(659, a);
        }
        static void F5(int a)
        {
            Console.Beep(698, a);
        }
        static void F5s(int a)
        {
            Console.Beep(739, a);
        }
        static void G5(int a)
        {
            Console.Beep(783, a);
        }
        static void G5s(int a)
        {
            Console.Beep(830, a);
        }
        static void A5(int a)
        {
            Console.Beep(880, a);
        }
        static void A5s(int a)
        {
            Console.Beep(932, a);
        }
        static void B5(int a)
        {
            Console.Beep(987, a);
        }
        static void C6(int a)
        {
            Console.Beep(1046, a);
        }
        static void C6s(int a)
        {
            Console.Beep(1108, a);
        }
        static void D6(int a)
        {
            Console.Beep(1174, a);
        }
        static void D6s(int a)
        {
            Console.Beep(1244, a);
        }
        static void E6(int a)
        {
            Console.Beep(1318, a);
        }
        static void F6(int a)
        {
            Console.Beep(1396, a);
        }
        static void F6s(int a)
        {
            Console.Beep(1479, a);
        }
        static void G6(int a)
        {
            Console.Beep(1567, a);
        }
        static void G6s(int a)
        {
            Console.Beep(1661, a);
        }
        static void A6(int a)
        {
            Console.Beep(1760, a);
        }
        static void A6s(int a)
        {
            Console.Beep(1864, a);
        }
        static void B6(int a)
        {
            Console.Beep(1975, a);
        }
        static void C7(int a)
        {
            Console.Beep(2093, a);
        }


    }
}