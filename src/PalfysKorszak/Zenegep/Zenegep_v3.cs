﻿using System;

namespace PalfysKorszak.Zenegep
{
    class Zenegep_v3
    {
        static void Run()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.CursorVisible = false;
            int hossz = 0;
            Console.Write("Add meg a hangok hosszát (ajánlott 300): ");
            while (true)
            {
                try
                {
                    hossz = int.Parse(Console.ReadLine());
                    break;
                }
                catch { }
            }
            Info();
            bool ki = false;
            while (!ki)
            {
                ConsoleKeyInfo gomb = Console.ReadKey();
                #region _shift + betű_
                if (gomb.Modifiers == ConsoleModifiers.Shift) //ha oktávot akar felfelé váltani
                {
                    switch (gomb.Key)
                    {
                        case ConsoleKey.Escape: ki = true; break;
                        case ConsoleKey.A: C6(hossz); break;
                        case ConsoleKey.W: C6s(hossz); break;
                        case ConsoleKey.S: D6(hossz); break;
                        case ConsoleKey.E: D6s(hossz); break;
                        case ConsoleKey.D: E6(hossz); break;
                        case ConsoleKey.F: F6(hossz); break;
                        case ConsoleKey.T: F6s(hossz); break;
                        case ConsoleKey.G: G6(hossz); break;
                        case ConsoleKey.Z: G6s(hossz); break;
                        case ConsoleKey.H: A6(hossz); break;
                        case ConsoleKey.U: A6s(hossz); break;
                        case ConsoleKey.J: B6(hossz); break;
                        case ConsoleKey.K: C7(hossz); break;
                        case ConsoleKey.O: C7s(hossz); break;
                        case ConsoleKey.L: D7(hossz); break;
                        case ConsoleKey.P: D7s(hossz); break;
                    }
                }
                #endregion _shift + betű - oktávfel_
                #region _alt + betű_
                else if (gomb.Modifiers == ConsoleModifiers.Alt) //ha oktávot akar lefelé váltani
                {
                    switch (gomb.Key)
                    {
                        case ConsoleKey.Escape: ki = true; break;
                        case ConsoleKey.A: C4(hossz); break;
                        case ConsoleKey.W: C4s(hossz); break;
                        case ConsoleKey.S: D4(hossz); break;
                        case ConsoleKey.E: D4s(hossz); break;
                        case ConsoleKey.D: E4(hossz); break;
                        case ConsoleKey.F: F4(hossz); break;
                        case ConsoleKey.T: F4s(hossz); break;
                        case ConsoleKey.G: G4(hossz); break;
                        case ConsoleKey.Z: G4s(hossz); break;
                        case ConsoleKey.H: A4(hossz); break;
                        case ConsoleKey.U: A4s(hossz); break;
                        case ConsoleKey.J: B4(hossz); break;
                        case ConsoleKey.K: C5(hossz); break;
                        case ConsoleKey.O: C5s(hossz); break;
                        case ConsoleKey.L: D5(hossz); break;
                        case ConsoleKey.P: D5s(hossz); break;
                    }
                }
                #endregion _ctrl + betű - oktávle_
                #region _sima_
                else //sima
                {
                    switch (gomb.Key)
                    {
                        case ConsoleKey.Escape: ki = true; break;
                        case ConsoleKey.F1: Demok(); Info(); break;
                        case ConsoleKey.A: C5(hossz); break;
                        case ConsoleKey.W: C5s(hossz); break;
                        case ConsoleKey.S: D5(hossz); break;
                        case ConsoleKey.E: D5s(hossz); break;
                        case ConsoleKey.D: E5(hossz); break;
                        case ConsoleKey.F: F5(hossz); break;
                        case ConsoleKey.T: F5s(hossz); break;
                        case ConsoleKey.G: G5(hossz); break;
                        case ConsoleKey.Z: G5s(hossz); break;
                        case ConsoleKey.H: A5(hossz); break;
                        case ConsoleKey.U: A5s(hossz); break;
                        case ConsoleKey.J: B5(hossz); break;
                        case ConsoleKey.K: C6(hossz); break;
                        case ConsoleKey.O: C6s(hossz); break;
                        case ConsoleKey.L: D6(hossz); break;
                        case ConsoleKey.P: D6s(hossz); break;
                    }
                }
                #endregion _sima_
            }
        }
        static void Info()
        {
            Console.Clear();
            Console.Title = "Zenegép v3.0";
            Console.Write("Félhangok:");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("\t W E - T Z U - O P\n");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Hangok:\t\tA S D F G H J K L");
            Console.WriteLine("Módosítók:\n\tshift: egyet fel\n\talt:   egyet le");
            Console.WriteLine("Demok: F1\tEscape: Kilépés");
            Console.WriteLine("==================================\n");
        }
        static void Demok()
        {
            Console.Clear();
            Console.WriteLine("Demok:");
            Console.WriteLine("1 - Für Elise\n2 - Boci-boci tarka");
            Console.WriteLine("Esc - Vissza");

            ConsoleKeyInfo demogomb;
            bool kidemo = false;
            while (!kidemo)
            {
                demogomb = Console.ReadKey();
                switch (demogomb.Key)
                {
                    case ConsoleKey.Escape: kidemo = true; break;
                    case ConsoleKey.D1: Fürelise(); break;
                    case ConsoleKey.D2: Bocibocitarka(); break;
                }
            }
        }
        static void Fürelise()
        {
            int hossz1 = 400, hossz2 = 800;
            E5(hossz1); D5s(hossz1); E5(hossz1); D5s(hossz1); E5(hossz1); B4(hossz1);
            D5(hossz1); C5(hossz1); A4(hossz2);

            C4(hossz1); E4(hossz1); A4(hossz1); B4(hossz2);

            E4(hossz1); G4s(hossz1); B4(hossz1); C5(hossz2); E4(hossz1);

            E5(hossz1); D5s(hossz1); E5(hossz1); D5s(hossz1); E5(hossz1); B4(hossz1);
            D5(hossz1); C5(hossz1); A4(hossz2); C4(hossz1); E4(hossz1); A4(hossz1);
            B4(hossz2);

            E4(hossz1); C5(hossz1); B4(hossz1); A4(hossz2);

            B4(hossz1); C5(hossz1); D5(hossz1); E5(hossz2); G4(hossz1); F5(hossz1);
            E5(hossz1); D5(hossz2); G4(hossz1); E5(hossz1); D5(hossz1); C5(hossz2);
            G4(hossz1); D5(hossz1); C5(hossz1); B4(hossz2); E4(hossz1); E5(hossz1);
            D5s(hossz1); E5(hossz1); D5s(hossz1); E5(hossz1); B4(hossz1); D5(hossz1);
            C5(hossz1); A4(hossz2);

            E4(hossz1); G4s(hossz1); A4(hossz1); B4(hossz2);

            E4(hossz1); C5(hossz1); B4(hossz1); A4(hossz2);
        }
        static void Bocibocitarka()
        {
            int hossz1 = 400, hossz2 = 800;
            C5(hossz1); E5(hossz1); C5(hossz1); E5(hossz1); G5(hossz2); G5(hossz2);
            C5(hossz1); E5(hossz1); C5(hossz1); E5(hossz1); G5(hossz2); G5(hossz2);
            C6(hossz1); B5(hossz1); A5(hossz1); G5(hossz1); F5(hossz2); A5(hossz2);
            G5(hossz1); F5(hossz1); E5(hossz1); D5(hossz1); C5(hossz2); C5(hossz2);
        }
        #region _HANGOK C4-től B7-ig_
        static void C4(int a)
        {
            Console.Beep(261, a);
        }
        static void C4s(int a)
        {
            Console.Beep(277, a);
        }
        static void D4(int a)
        {
            Console.Beep(293, a);
        }
        static void D4s(int a)
        {
            Console.Beep(311, a);
        }
        static void E4(int a)
        {
            Console.Beep(329, a);
        }
        static void F4(int a)
        {
            Console.Beep(349, a);
        }
        static void F4s(int a)
        {
            Console.Beep(369, a);
        }
        static void G4(int a)
        {
            Console.Beep(391, a);
        }
        static void G4s(int a)
        {
            Console.Beep(415, a);
        }
        static void A4(int a)
        {
            Console.Beep(440, a);
        }
        static void A4s(int a)
        {
            Console.Beep(466, a);
        }
        static void B4(int a)
        {
            Console.Beep(493, a);
        }
        static void C5(int a)
        {
            Console.Beep(523, a);
        }
        static void C5s(int a)
        {
            Console.Beep(554, a);
        }
        static void D5(int a)
        {
            Console.Beep(587, a);
        }
        static void D5s(int a)
        {
            Console.Beep(622, a);
        }
        static void E5(int a)
        {
            Console.Beep(659, a);
        }
        static void F5(int a)
        {
            Console.Beep(698, a);
        }
        static void F5s(int a)
        {
            Console.Beep(739, a);
        }
        static void G5(int a)
        {
            Console.Beep(783, a);
        }
        static void G5s(int a)
        {
            Console.Beep(830, a);
        }
        static void A5(int a)
        {
            Console.Beep(880, a);
        }
        static void A5s(int a)
        {
            Console.Beep(932, a);
        }
        static void B5(int a)
        {
            Console.Beep(987, a);
        }
        static void C6(int a)
        {
            Console.Beep(1046, a);
        }
        static void C6s(int a)
        {
            Console.Beep(1108, a);
        }
        static void D6(int a)
        {
            Console.Beep(1175, a);
        }
        static void D6s(int a)
        {
            Console.Beep(1245, a);
        }
        static void E6(int a)
        {
            Console.Beep(1318, a);
        }
        static void F6(int a)
        {
            Console.Beep(1396, a);
        }
        static void F6s(int a)
        {
            Console.Beep(1479, a);
        }
        static void G6(int a)
        {
            Console.Beep(1567, a);
        }
        static void G6s(int a)
        {
            Console.Beep(1661, a);
        }
        static void A6(int a)
        {
            Console.Beep(1760, a);
        }
        static void A6s(int a)
        {
            Console.Beep(1864, a);
        }
        static void B6(int a)
        {
            Console.Beep(1975, a);
        }
        static void C7(int a)
        {
            Console.Beep(2093, a);
        }
        static void C7s(int a)
        {
            Console.Beep(2217, a);
        }
        static void D7(int a)
        {
            Console.Beep(2349, a);
        }
        static void D7s(int a)
        {
            Console.Beep(2489, a);
        }
        static void E7(int a)
        {
            Console.Beep(2637, a);
        }
        static void F7(int a)
        {
            Console.Beep(2794, a);
        }
        static void F7s(int a)
        {
            Console.Beep(2960, a);
        }
        static void G7(int a)
        {
            Console.Beep(3136, a);
        }
        static void G7s(int a)
        {
            Console.Beep(3322, a);
        }
        static void A7(int a)
        {
            Console.Beep(3520, a);
        }
        static void A7s(int a)
        {
            Console.Beep(3729, a);
        }
        static void B7(int a)
        {
            Console.Beep(3951, a);
        }
        #endregion _HANGOK C4-től B7-ig_
    }
    /*
    Changelog:
     * 1.0 - alapötlet, beírjuk egy sorba, majd lejátsza.
     * 2.0 - már gombnyomós, 300 ms ideig megy egy hang, félhangok berakása
     * 3.0 - a hangok megjelenítése piros, és az elhelyezkedésük is jó
           - Ctrl helyet Alt az alsó módosító a hibák miatt
           - Info() megalkotása
           - További hangok és gombok hozzáadása
           - választható hanghossz hozzáadása kivételkezeléssel
           - demorendszer kialakítása
    */
}