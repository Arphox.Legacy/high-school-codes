﻿using System;
using System.Threading;

namespace PalfysKorszak.Zenegep
{
    class Zenegep_v2
    {
        static void Run()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.CursorVisible = false;
            Console.Title = "Zenegép v2.0";
            Console.WriteLine("Hangok: a-s-d-f-g-h-j-k\nMódosítók:\n\tshift: egyet fel\n\talt:   egyet le");
            Console.WriteLine("DEMO: F1 - Für Elise\n");
            int hossz = 300;
            bool ki = false;
            while (!ki)
            {
                ConsoleKeyInfo gomb = Console.ReadKey();
                #region _shift + betű_
                if (gomb.Modifiers == ConsoleModifiers.Shift) //ha oktávot akar felfelé váltani
                {
                    switch (gomb.Key)
                    {
                        case ConsoleKey.Escape: ki = true; break;
                        case ConsoleKey.A: C6(hossz); break;
                        case ConsoleKey.W: C6s(hossz); break;
                        case ConsoleKey.S: D6(hossz); break;
                        case ConsoleKey.E: D6s(hossz); break;
                        case ConsoleKey.D: E6(hossz); break;
                        case ConsoleKey.F: F6(hossz); break;
                        case ConsoleKey.T: F6s(hossz); break;
                        case ConsoleKey.G: G6(hossz); break;
                        case ConsoleKey.Z: G6s(hossz); break;
                        case ConsoleKey.H: A6(hossz); break;
                        case ConsoleKey.U: A6s(hossz); break;
                        case ConsoleKey.J: B6(hossz); break;
                        case ConsoleKey.K: C7(hossz); break;
                    }
                }
                #endregion _shift + betű - oktávfel_
                #region _alt + betű - oktávle_
                else if (gomb.Modifiers == ConsoleModifiers.Alt) //ha oktávot akar lefelé váltani
                {
                    switch (gomb.Key)
                    {
                        case ConsoleKey.Escape: ki = true; break;
                        case ConsoleKey.A: C4(hossz); break;
                        case ConsoleKey.W: C4s(hossz); break;
                        case ConsoleKey.S: D4(hossz); break;
                        case ConsoleKey.E: D4s(hossz); break;
                        case ConsoleKey.D: E4(hossz); break;
                        case ConsoleKey.F: F4(hossz); break;
                        case ConsoleKey.T: F4s(hossz); break;
                        case ConsoleKey.G: G4(hossz); break;
                        case ConsoleKey.Z: G4s(hossz); break;
                        case ConsoleKey.H: A4(hossz); break;
                        case ConsoleKey.U: A4s(hossz); break;
                        case ConsoleKey.J: B4(hossz); break;
                        case ConsoleKey.K: C5(hossz); break;
                    }
                }
                #endregion _ctrl + betű - oktávle_
                #region _sima_
                else //sima
                {
                    switch (gomb.Key)
                    {
                        case ConsoleKey.Escape: ki = true; break;
                        case ConsoleKey.F1: Fürelise(); break;
                        case ConsoleKey.A: C5(hossz); break;
                        case ConsoleKey.W: C5s(hossz); break;
                        case ConsoleKey.S: D5(hossz); break;
                        case ConsoleKey.E: D5s(hossz); break;
                        case ConsoleKey.D: E5(hossz); break;
                        case ConsoleKey.F: F5(hossz); break;
                        case ConsoleKey.T: F5s(hossz); break;
                        case ConsoleKey.G: G5(hossz); break;
                        case ConsoleKey.Z: G5s(hossz); break;
                        case ConsoleKey.H: A5(hossz); break;
                        case ConsoleKey.U: A5s(hossz); break;
                        case ConsoleKey.J: B5(hossz); break;
                        case ConsoleKey.K: C6(hossz); break;
                    }
                }
                #endregion _sima_
            }
        }
        static void Fürelise()
        {
            Console.Clear();
            Console.WriteLine("F1 - Für Elise DEMO");
            int hossz1 = 400, hossz2 = 800;
            E5(hossz1);
            D5s(hossz1);
            E5(hossz1);
            D5s(hossz1);
            E5(hossz1);
            B4(hossz1);
            D5(hossz1);
            C5(hossz1);
            A4(hossz2);

            C4(hossz1);
            E4(hossz1);
            A4(hossz1);
            B4(hossz2);

            E4(hossz1);
            G4s(hossz1);
            B4(hossz1);
            C5(hossz2);

            E4(hossz1);

            E5(hossz1);
            D5s(hossz1);
            E5(hossz1);
            D5s(hossz1);
            E5(hossz1);
            B4(hossz1);
            D5(hossz1);
            C5(hossz1);
            A4(hossz2);
            C4(hossz1);
            E4(hossz1);
            A4(hossz1);
            B4(hossz2);

            E4(hossz1);
            C5(hossz1);
            B4(hossz1);
            A4(hossz2);

            B4(hossz1);
            C5(hossz1);
            D5(hossz1);
            E5(hossz2);
            G4(hossz1);
            F5(hossz1);
            E5(hossz1);
            D5(hossz2);
            G4(hossz1);
            E5(hossz1);
            D5(hossz1);
            C5(hossz2);
            G4(hossz1);
            D5(hossz1);
            C5(hossz1);
            B4(hossz2);
            E4(hossz1);
            E5(hossz1);
            D5s(hossz1);
            E5(hossz1);
            D5s(hossz1);
            E5(hossz1);
            B4(hossz1);
            D5(hossz1);
            C5(hossz1);
            A4(hossz2);

            E4(hossz1);
            G4s(hossz1);
            A4(hossz1);
            B4(hossz2);

            E4(hossz1);
            C5(hossz1);
            B4(hossz1);
            A4(hossz2);
            Console.WriteLine("DEMO vége.");
            Thread.Sleep(1000);
        }
        // C    D   E   F   G   A   B   C
        // dó   ré  mi  fá  szó lá  ti  dó
        #region _HANGOK C4-től C7-ig_
        static void C4(int a)
        {
            Console.Beep(261, a);
        }
        static void C4s(int a)
        {
            Console.Beep(277, a);
        }
        static void D4(int a)
        {
            Console.Beep(293, a);
        }
        static void D4s(int a)
        {
            Console.Beep(311, a);
        }
        static void E4(int a)
        {
            Console.Beep(329, a);
        }
        static void F4(int a)
        {
            Console.Beep(349, a);
        }
        static void F4s(int a)
        {
            Console.Beep(369, a);
        }
        static void G4(int a)
        {
            Console.Beep(391, a);
        }
        static void G4s(int a)
        {
            Console.Beep(415, a);
        }
        static void A4(int a)
        {
            Console.Beep(440, a);
        }
        static void A4s(int a)
        {
            Console.Beep(466, a);
        }
        static void B4(int a)
        {
            Console.Beep(493, a);
        }
        static void C5(int a)
        {
            Console.Beep(523, a);
        }
        static void C5s(int a)
        {
            Console.Beep(554, a);
        }
        static void D5(int a)
        {
            Console.Beep(587, a);
        }
        static void D5s(int a)
        {
            Console.Beep(622, a);
        }
        static void E5(int a)
        {
            Console.Beep(659, a);
        }
        static void F5(int a)
        {
            Console.Beep(698, a);
        }
        static void F5s(int a)
        {
            Console.Beep(739, a);
        }
        static void G5(int a)
        {
            Console.Beep(783, a);
        }
        static void G5s(int a)
        {
            Console.Beep(830, a);
        }
        static void A5(int a)
        {
            Console.Beep(880, a);
        }
        static void A5s(int a)
        {
            Console.Beep(932, a);
        }
        static void B5(int a)
        {
            Console.Beep(987, a);
        }
        static void C6(int a)
        {
            Console.Beep(1046, a);
        }
        static void C6s(int a)
        {
            Console.Beep(1108, a);
        }
        static void D6(int a)
        {
            Console.Beep(1174, a);
        }
        static void D6s(int a)
        {
            Console.Beep(1244, a);
        }
        static void E6(int a)
        {
            Console.Beep(1318, a);
        }
        static void F6(int a)
        {
            Console.Beep(1396, a);
        }
        static void F6s(int a)
        {
            Console.Beep(1479, a);
        }
        static void G6(int a)
        {
            Console.Beep(1567, a);
        }
        static void G6s(int a)
        {
            Console.Beep(1661, a);
        }
        static void A6(int a)
        {
            Console.Beep(1760, a);
        }
        static void A6s(int a)
        {
            Console.Beep(1864, a);
        }
        static void B6(int a)
        {
            Console.Beep(1975, a);
        }
        static void C7(int a)
        {
            Console.Beep(2093, a);
        }
        #endregion _HANGOK C4-től C7-ig_

    }
}