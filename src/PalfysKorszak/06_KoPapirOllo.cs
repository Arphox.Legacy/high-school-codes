﻿using System;

namespace PalfysKorszak
{
    class KoPapirOllo
    {
        static void Run()
        {
            string jatekos = null, gep = null;
            byte jatekoseredmeny = 0, geperedmeny = 0;
            Random r = new Random();
            bool ki = false;
            while (!ki)
            {
                char v = ' ';
                do
                {
                    Console.Clear();
                    Console.WriteLine("Mit választasz? (k/p/o)");
                    v = Console.ReadKey(true).KeyChar;
                    switch (v)
                    {
                        case 'k': jatekos = "kő"; break;
                        case 'p': jatekos = "papír"; break;
                        case 'o': jatekos = "olló"; break;
                    }
                }
                while (v != 'k' && v != 'p' && v != 'o');
                switch (r.Next(0, 3))
                {
                    case 0: gep = "kő"; break;
                    case 1: gep = "papír"; break;
                    case 2: gep = "olló"; break;
                }
                if ((gep == "kő" && jatekos == "olló") || (gep == "papír" && jatekos == "kő") || (gep == "olló" && jatekos == "papír"))
                    Console.WriteLine("Vesztettél!\nAz állás:\nJátékos: {0}\tGép: {1}", jatekoseredmeny, ++geperedmeny);
                else if (gep == jatekos)
                    Console.WriteLine("Döntetlen!\nAz állás:\nJátékos: {0}\tGép: {1}", jatekoseredmeny, geperedmeny);
                else
                    Console.WriteLine("Nyertél!\nAz állás:\nJátékos: {0}\tGép: {1}", ++jatekoseredmeny, geperedmeny);
                Console.WriteLine("\n\nAkarsz még játszani? (i/n)");
                char akare = Console.ReadKey(true).KeyChar;
                if (akare == 'n') { ki = true; }
            }
        }
    }
}