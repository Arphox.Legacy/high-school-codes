﻿using System;

namespace PalfysKorszak
{
    class MatrixHullas
    {
        static void Run()
        {
            Random random = new Random();
            Console.CursorVisible = false;
            Console.ForegroundColor = ConsoleColor.Green;
            for (int a = 0; a < 100000; a++)
            {
                int c1 = random.Next(10);
                int c2 = random.Next(10);
                Console.SetCursorPosition(c1, c2);
                int spacerandom = random.Next(60);
                for (int b = 0; b < 20; b++)
                {
                    for (int space = 0; space < spacerandom; space++)
                        Console.Write(" ");
                    Console.Write("0");
                    c2++;
                    Console.SetCursorPosition(c1, c2);
                    int l = 0;
                    for (int c = 0; c < 20000000; c++)
                        l++;
                }
                Console.Clear();
            }
        }
    }
}