﻿using System;

namespace PalfysKorszak
{
    class DeterminansMegoldo_Otodev
    {
        static void Run()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.CursorVisible = false;
            string[] menupontok = new string[3];
            menupontok[0] = " 2 x 2 ";
            menupontok[1] = " 3 x 3 ";
            menupontok[2] = " Kilépés ";
            int válasz;
            do
            {
                válasz = menuhozkell(menupontok);
                switch (válasz)
                {
                    case 0: Ketszerkettes(); break;
                    case 1: Haromszorharmas(); break;
                }
            } while (válasz != 2);
        }
        static void Ketszerkettes()
        {
            Console.Clear();
            Console.CursorVisible = true;
            Console.WriteLine("Add meg a 2x2-es mátrixot a következő formában:");
            Console.WriteLine("1 2\n3 4\n====\n");
            string[] sor1 = Console.ReadLine().Split(' ');
            string[] sor2 = Console.ReadLine().Split(' ');
            Console.Write("Add meg a mátrix szorzóját (ha nincs, nyomj entert): ");
            string szorzo = Console.ReadLine();
            double[,] m = new double[2, 2]; //m, mint mátrix
            m[0, 0] = Convert.ToDouble(sor1[0]);
            m[0, 1] = Convert.ToDouble(sor1[1]);
            m[1, 0] = Convert.ToDouble(sor2[0]);
            m[1, 1] = Convert.ToDouble(sor2[1]);
            double determinans = Masodfoku(m[0, 0], m[0, 1], m[1, 0], m[1, 1]);
            if (szorzo != "")
                determinans *= Convert.ToDouble(szorzo);
            Console.WriteLine("A determináns: " + determinans);
            Console.ReadLine();
        }
        static void Haromszorharmas()
        {
            Console.Clear();
            Console.CursorVisible = true;
            Console.WriteLine("Add meg a 3x3-as mátrixot a következő formában:");
            Console.WriteLine("1 2 3\n4 5 6\n7 8 9\n=====\n");
            string[] sor1 = Console.ReadLine().Split(' ');
            string[] sor2 = Console.ReadLine().Split(' ');
            string[] sor3 = Console.ReadLine().Split(' ');
            double[,] m = new double[3, 3]; //m, mint mátrix
            m[0, 0] = Convert.ToDouble(sor1[0]);
            m[0, 1] = Convert.ToDouble(sor1[1]);
            m[0, 2] = Convert.ToDouble(sor1[2]);
            m[1, 0] = Convert.ToDouble(sor2[0]);
            m[1, 1] = Convert.ToDouble(sor2[1]);
            m[1, 2] = Convert.ToDouble(sor2[2]);
            m[2, 0] = Convert.ToDouble(sor3[0]);
            m[2, 1] = Convert.ToDouble(sor3[1]);
            m[2, 2] = Convert.ToDouble(sor3[2]);
            Console.Write("Add meg a mátrix szorzóját (ha nincs, nyomj entert): ");
            string szorzo = Console.ReadLine();
            double elsoresz = m[0, 0] * Masodfoku(m[1, 1], m[1, 2], m[2, 1], m[2, 2]);
            double masodikresz = m[1, 0] * Masodfoku(m[0, 1], m[0, 2], m[2, 1], m[2, 2]);
            double harmadikresz = m[2, 0] * Masodfoku(m[0, 1], m[0, 2], m[1, 1], m[1, 2]);
            double determinans = elsoresz - masodikresz + harmadikresz;
            if (szorzo != "")
                determinans *= Convert.ToDouble(szorzo);
            Console.WriteLine("A determináns: " + determinans);
            Console.ReadLine();
        }
        static int menuhozkell(string[] menupontok)
        {
            Console.Clear();
            int aktuálismenüpont = 0;
            ConsoleKeyInfo gomb;
            do
            {
                for (int i = 0; i < menupontok.Length; i++) //ez a rész kiírja a menüpontokat
                {
                    Console.SetCursorPosition(0, i);
                    if (aktuálismenüpont == i)
                        Console.BackgroundColor = ConsoleColor.Blue;
                    else
                        Console.BackgroundColor = ConsoleColor.Black;
                    Console.Write(menupontok[i]);
                }
                Console.BackgroundColor = ConsoleColor.Black;
                gomb = Console.ReadKey(true);
                if (gomb.Key == ConsoleKey.DownArrow)
                    aktuálismenüpont++;
                else if (gomb.Key == ConsoleKey.UpArrow)
                    aktuálismenüpont--;
                else if (gomb.Key == ConsoleKey.Enter)
                    return aktuálismenüpont;

                if (aktuálismenüpont < 0) //ha felfelé "kifutnánk"
                    aktuálismenüpont = menupontok.Length - 1;
                else if (aktuálismenüpont > menupontok.Length - 1) //ha lefelé "kifutnánk"
                    aktuálismenüpont = 0;
            } while (gomb.Key != ConsoleKey.Enter);
            return -1;
        }
        static double Masodfoku(double balfent, double jobbfent, double ballent, double jobblent)
        {
            double det = (balfent * jobblent) - (jobbfent * ballent);
            return det;
        }
    }
}