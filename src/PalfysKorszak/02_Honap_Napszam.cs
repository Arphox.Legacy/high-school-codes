﻿using System.IO;

namespace PalfysKorszak
{
    class Honap_Napszam
    {
        /*
         * A program készít egy Hónapok.txt fájlt,
         * ami kiírja minden sorban a hónap nevét, az aktuális napot, és utána tesz egy pontot! Minta:
         Január 1.
         Január 2.
         Január 3.
         * stb. A program annyi napot írjon ki, amennyi nap ténylegesen van egy hónapban!
        */

        static void Run()
        {
            string[] honap = new string[] { "Január", "Február", "Március", "Április", "Május", "Június", "Július", "Augusztus", "Szeptember", "Október", "November", "December" };
            StreamWriter sw = new StreamWriter("Hónapok.txt");
            for (int i = 0; i < 12; i++)
            {
                int a = 31;
                if (honap[i] == "Április" || honap[i] == "Június" || honap[i] == "Szeptember" || honap[i] == "November")
                    a = 30;
                if (honap[i] == "Február")
                    a = 28;
                for (int k = 1; k <= a; k++)
                {
                    sw.WriteLine("{0} {1}.", honap[i], k);
                }
            }
            sw.Close();
        }
    }
}