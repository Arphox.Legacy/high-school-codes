﻿using System.IO;

namespace PalfysKorszak
{
    class Honap_Napnev
    {
        /*
         * A program készít egy Hónapok.txt fájlt,
         * ami kiírja minden sorban a hónap nevét és az aktuális nap nevét! Minta:
         Január elseje
         Január másodika
         Január harmadika
         * stb. A program annyi napot írjon ki, amennyi nap ténylegesen van egy hónapban!
        */

        static void Run()
        {
            string[] honap = new string[] { "Január", "Február", "Március", "Április", "Május", "Június", "Július", "Augusztus", "Szeptember", "Október", "November", "December" };
            string[] napneve = new string[] { "egyedike", "kettedike", "harmadika", "negyedike", "ötödike", "hatodika", "hetedike", "nyolcadika", "kilencedike", "tizedike", "huszadika", "harmincadika" };
            string kiirando = ""; //ezt fogja kiírni a hónap után

            StreamWriter sw = new StreamWriter("Hónapok.txt");
            for (int i = 0; i < 12; i++) //végigmegy minden hónapon
            {
                int napszam = 31; //napszám
                if (honap[i] == "Április" || honap[i] == "Június" || honap[i] == "Szeptember" || honap[i] == "November")
                    napszam = 30; //áp-jun-szept-nov: 30 naposak
                if (honap[i] == "Február")
                    napszam = 28; //február 28 napos
                for (int k = 1; k <= napszam; k++)
                {
                    if (k == 1)
                        kiirando = "elseje";
                    if (k == 2)
                        kiirando = "másodika";
                    if (k > 2 && k <= 10)
                        kiirando = napneve[k - 1];
                    if (k >= 11 && k <= 19)
                        kiirando = "tizen" + napneve[k - 11];
                    if (k == 20)
                        kiirando = napneve[10];
                    if (k >= 21 && k <= 29)
                        kiirando = "huszon" + napneve[k - 21];
                    if (k == 30)
                        kiirando = napneve[11];
                    if (k == 31)
                        kiirando = "harmincegyedike";
                    sw.WriteLine("{0} {1}", honap[i], kiirando);
                }
                if (i < 11) //hónapok végén rak entert, az utolsó után nem.
                    sw.WriteLine();
            }
            sw.Close();
        }
    }
}
