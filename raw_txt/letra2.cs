using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace letra2
{
    class Program 
    {
        static int a = 0, b = 0, osszeg = 0, hatarertek = 0;
        static bool nyertes = false, ellenorzes = false;
        static string ajatekos = "", bjatekos = "";

        static void Main(string[] args) 
        {
            Kezdes();

            Console.WriteLine(ajatekos + "\t\t" + bjatekos + "\n");
            do
            {
                try
                {
                    a = Convert.ToInt32(Console.ReadLine());
                    while (a < 1 || a > 10)
                    {
                        Console.WriteLine("Csak 1 �s 10 k�z�tti sz�mot �rhatsz be!");
                        a = Convert.ToInt32(Console.ReadLine());
                    }
                }
                catch (Exception)
                {
                    do
                    {
                        Console.WriteLine("Csak 1 �s 10 k�z�tti sz�mot �rhatsz be!");
                        try
                        {
                            a = Convert.ToInt32(Console.ReadLine());
                            ellenorzes = true;
                        }
                        catch (Exception)
                        {
                            ellenorzes = false;
                        }
                    }
                    while (a < 1 || a > 10 && ellenorzes == true);
                }
                osszeg = osszeg + a;
                if (osszeg > hatarertek)
                {
                    Console.WriteLine("\nT�ll�pted a "+hatarertek+"-t, a m�sik j�t�kos nyert!");
                    break;
                }
                if (osszeg == hatarertek)
                {
                    nyertes = false; //false akkor, ha az A-j�t�kos nyer.
                    break;
                }
                Console.WriteLine("\t" + osszeg); Console.Write("\t\t");
                try
                {
                    b = Convert.ToInt32(Console.ReadLine());
                    while (b < 1 || b > 10)
                    {
                        Console.WriteLine("Csak 1 �s 10 k�z�tti sz�mot �rhatsz be!"); Console.Write("\t\t");
                        b = Convert.ToInt32(Console.ReadLine());
                    }
                }
                catch (Exception)
                {
                    do
                    {
                        Console.WriteLine("Csak 1 �s 10 k�z�tti sz�mot �rhatsz be!"); Console.Write("\t\t");
                        try
                        {
                            b = Convert.ToInt32(Console.ReadLine());
                            ellenorzes = true;
                        }
                        catch (Exception)
                        {
                            ellenorzes = false;
                        }
                    }
                    while (b < 1 || b > 10 && ellenorzes == true);
                }
                osszeg = osszeg + b;
                if (osszeg > hatarertek)
                {
                    Console.WriteLine("\nT�ll�pted a " + hatarertek + "-t, a m�sik j�t�kos nyert!");
                    break;
                }
                if (osszeg == hatarertek)
                {
                    nyertes = true; //true akkor, ha a B-j�t�kos nyer.
                    break;
                }
                Console.WriteLine("\t" + osszeg);
            }
            while (true);
            if (nyertes == false)
            {
                Console.WriteLine("\t"+hatarertek);
                Console.WriteLine("\nA-j�t�kos: " + ajatekos + " nyert!");
            }
            else
            {
                Console.WriteLine("\t"+hatarertek);
                Console.WriteLine("\nB-j�t�kos: " + bjatekos + " nyert!");
            }
            Console.ReadKey();
        }
        static void Kezdes() 
        {
            Console.Clear();
            Console.WriteLine("�dv�z�llek a L�tra logikai j�t�kban!");
            Console.WriteLine("==============================================\nJ�t�kszab�lyok:\n");
            Console.WriteLine("Ketten j�tsz�k a j�t�kot. A-j�t�kos �s B-j�t�kos.\nFelv�ltva mondanak egy sz�mot 1-10-ig. A g�p a sz�mokat �sszeadja.");
            Console.WriteLine("Aki hamarabb el�ri a hat�r�rt�ket, az nyer.");
            Console.WriteLine("==============================================\n");
            Console.Write("Adja meg a j�t�kosok neveit!\nA-j�t�kos neve: ");
            ajatekos = Convert.ToString(Console.ReadLine());
            Console.Write("B-j�t�kos neve: ");
            bjatekos = Convert.ToString(Console.ReadLine());

            Console.Write("Adj meg egy hat�r�rt�ket 20 �s 100 k�z�tt: ");
            do
            {
                try
                {
                    hatarertek = Convert.ToInt32(Console.ReadLine());
                    if (hatarertek < 20 || hatarertek > 100)
                    {
                        Console.Write("Rossz a megadott sz�m, adj meg egy m�sikat: ");
                    }
                }
                catch (Exception)
                {
                    Console.Write("Rossz a megadott sz�m, adj meg egy m�sikat: ");
                }
            }
            while (hatarertek < 20 || hatarertek > 100);
            Console.Clear();
        }
    }
}
