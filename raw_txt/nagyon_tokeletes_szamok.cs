using System;

class Program
{
    static void Main()
    {
        /*
         * Nagyon t�k�letes sz�mok.
         * Suryanarayana indiai matematikus vezette be ezt a fogalmat.
         * Nagyon t�k�letesnek nevezte azt a sz�mot, amely oszt�i �sszeg�nek oszt�it �sszeadva,
         * a sz�m k�tszeres�t kapjuk. Ilyen p�ld�ul a 16, mert oszt�inak �sszege 31,
         * a 31 oszt�it �sszeadva pedig 32-t kapunk. A feladat teh�t:
         * keress�nk programmal nagyon t�k�letes sz�mokat!
        */
        Console.WriteLine("Nagyon t�k�letes sz�mok 100 000-ig:");
        for (int i = 0; i < 100000; i+=2) //0-t�l 100 000 -ig keres t�k�letes sz�mokat.
        {
            if (tokeletes(i) == true)
                Console.WriteLine(i);
        }
        Console.WriteLine("\nKeres�s v�ge.");
        Console.ReadLine();
    }
    static bool tokeletes(int a) //ez n�zi meg a sz�mr�l, hogy nagyon t�k�letes -e
    {
        int osszeg1 = 0, osszeg2 = 0;
        for (int i = 1; i <= a; i++) //�sszeadja az oszt�it
        {
            if (a % i == 0)
            {
                osszeg1 += i;
            }
        }
        for (int i = 1; i <= osszeg1; i++) //�sszeadja az oszt�i oszt�it
        {
            if (osszeg1 % i == 0)
            {
                osszeg2 += i;
            }
        }
        if (a == 0) //0-nak nincsenek oszt�i, mert a null�val val� oszt�s nem �rtelmezett
            return false;
        if (osszeg2 == a * 2) //Ha az a k�tszerese egyenl� ezzel, akkor igazzal t�r vissza.
            return true;
        else
            return false;
    }
}
