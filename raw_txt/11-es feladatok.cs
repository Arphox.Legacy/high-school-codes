using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _11_es_feladatok
{
    class Program
    {
        static bool ellenorzes = false;

        static void Main() 
        {
            /*
            8. feladat:
            Az els� h�t feladatot val�s�tsd meg men�szerkezettel egyetlen feladat form�j�ban!
            */
            do
            {
                Menu();
            }
            while (true);
        }
        static void Menu() 
        {
            Console.Clear();
            Console.WriteLine("8. feladat: Az els� h�t feladatot val�s�tsd meg men�szerkezettel egyetlen feladat form�j�ban!\nV�lasszon egy feladatsz�mot (1-7)!");
            Console.WriteLine("Kil�p�s: 0");
            string a = Convert.ToString(Console.ReadLine());
            switch (a)
            {
                case "1":
                    Elso();
                    break;
                case "2":
                    Masodik();
                    break;
                case "3":
                    Harmadik();
                    break;
                case "4":
                    Negyedik();
                    break;
                case "5":
                    Otodik();
                    break;
                case "6":
                    Hatodik();
                    break;
                case "7":
                    Hetedik();
                    break;
                case "0":
                    Environment.Exit(-1);
                    break;
                default:
                    break;
            }
        }
        static void Elso() 
        {
            /*
            1. feladat:
            Deklar�lj k�t int t�pus� v�ltoz�t!
            Mind a kett�ben helyezz el egy-egy �rt�ket, amelyet a felhaszn�l�t�l k�rsz be!
            �rasd ki a v�ltoz�k tartalm�t!
            Cser�ld meg a v�ltoz�k tartalm�t!
			�jra �rasd ki a v�ltoz�k tartalm�t!
            */

            Console.Clear();
            int a = 0, b = 0;
            szambekeres(ref a);
            do
            {
                try
                {
                    Console.Clear();
                    Console.Write("Adjon meg egy sz�mot: {0}", a);
                    Console.Write("\nAdjon meg egy sz�mot: ");
                    b = Convert.ToInt16(Console.ReadLine());
                    ellenorzes = true;
                }
                catch (Exception) { }
            }
            while (ellenorzes == false);

            Console.WriteLine("\nAz a = {0}, b = {1}", a, b);
            int temp = a; a = b; b = temp;
            Console.WriteLine("\nFelcser�lve az a = {0}, b = {1}", a, b);
            Console.ReadLine();
            Console.Clear();
        }
        static void Masodik() 
        {
            /*
            2. feladat:
            K�rj be a felhaszn�l�t�l egy eg�sz sz�mot!
            D�ntsd el, hogy a sz�m kisebb-e mint t�z, nagyobb-e mint t�z, vagy egyenl�-e t�zzel!
            A feladatot egym�sba �gyazott if-else szerkezetekkel val�s�tsd meg!
            �rd ki a megfelel� sz�veges �zenetet a felhaszn�l� r�sz�re!
            */

            Console.Clear();
            int a = 0;
            szambekeres(ref a);
            if (a <= 10)
            {
                if (a == 10)
                    Console.WriteLine("a = 10");
                else
                    Console.WriteLine("a < 10");
            }
            else
                Console.WriteLine("a > 10");
            
            Console.ReadKey();
        }
        static void Harmadik() 
        {
            /*
            3. feladat:
            K�rj be a felhaszn�l�t�l egy eg�sz sz�mot!
            D�ntsd el, hogy a sz�m nulla �s t�z k�z� esik-e, t�z �s h�sz k�z� esik-e,
            harminc �s negyven k�z� esik-e, vagy egy�b helyre esik-e!
            A feladatot egym�sba �gyazott if-else szerkezetekkel val�s�tsd meg!
            �rd ki a megfelel� sz�veges �zenetet a felhaszn�l� r�sz�re!
            */

            Console.Clear();
            int a = 0;
            szambekeres(ref a);

            if (a < 40)
            {
                if (a < 30)
                {
                    if (a < 20)
                    {
                        if (a < 10)
                        {
                            if (a < 0)
                            {
                                Console.WriteLine("a egy�b helyre esik");
                            }
                            else
                                Console.WriteLine("a 0 �s 10 k�z� esik");
                        }
                        else
                            Console.WriteLine("a 10 �s 20 k�z� esik");
                    }
                    else
                        Console.WriteLine("a 20 �s 30 k�z� esik");
                }
                else
                    Console.WriteLine("a 30 �s 40 k�z� esik");
            }
            else
                Console.WriteLine("a egy�b helyre esik");

            Console.ReadLine();
            Console.Clear();
        }
        static void Negyedik() 
        {
            /*
            4. feladat:
            K�rj be a felhaszn�l�t�l k�t val�s sz�mot!
		    K�sz�ts egy men�t, amely a k�vetkez�k�ppen n�z ki:
            1.	�sszead�s
            2.	Kivon�s
            3.	Szorz�s
            4.	Oszt�s
            K�rj be a felhaszn�l�t�l egy sz�mot egyt�l n�gyig!
            A program hajtsa v�gre a megfelel� opci�t!
            */

            Console.Clear();
            double a = 0, b = 0;
            valosbekeres(ref a);
            valosbekeres(ref b);

            Console.WriteLine("1. �sszead�s\n2. Kivon�s\n3. Szorz�s\n4. Oszt�s");
            ide: Console.WriteLine("Adja meg a m�velet sz�m�t!");
            string x = Convert.ToString(Console.ReadLine());
            switch (x)
            {
                case "1":
                    Console.WriteLine("{0} + {1} = {2}", a, b, (a + b));
                    Console.ReadLine();
                    break;
                case "2":
                    Console.WriteLine("{0} - {1} = {2}", a, b, (a - b));
                    break;
                case "3":
                    Console.WriteLine("{0} * {1} = {2}", a, b, a * b);
                    break;
                case "4":
                    if (b == 0)
                    {
                        do
                        {
                            try
                            {
                                Console.WriteLine("Null�val nem lehet osztani!\nAdja meg �jra a m�sodik (val�s) sz�mot! (tizedespont = vessz�)");
                                b = Convert.ToDouble(Console.ReadLine());
                                while (b == 0)
                                {
                                    Console.WriteLine("Null�val nem lehet osztani!\nAdja meg �jra a m�sodik (val�s) sz�mot! (tizedespont = vessz�)");
                                    b = Convert.ToInt32(Console.ReadLine());
                                }
                                ellenorzes = true;
                            }
                            catch (Exception) { }
                        }
                        while (ellenorzes == false);
                        Console.WriteLine("{0} / {1} = {2}", a, b, a / b);
                    }
                    else
                        Console.WriteLine("{0} / {1} = {2}", a, b, a / b);
                    break;
                default:
                    goto ide;
            }
            Console.ReadLine();
            Console.Clear();
        }
        static void Otodik() 
        {
            /*
            5. feladat:
            K�rj be a felhaszn�l�t�l egy sz�mot egyt�l huszonn�gyig!
		    K�rd be a felhaszn�l� nev�t!
		    K�sz�nj neki napszaknak megfelel�en n�vre sz�l�an!
		    6-8: �J� reggelt N�v!�
		    9-18: �J� napot N�v!�
		    19-22: �J� est�t N�v!�
            23-5: �J� �jszak�t N�v!�
            */

            Console.Clear();
            byte a = 0; string nev = null;
            do
            {
                try
                {
                    Console.WriteLine("Adjon meg egy sz�mot 1-t�l 24-ig!");
                    a = Convert.ToByte(Console.ReadLine());
                    while (a < 1 || a > 24)
                    {
                        Console.WriteLine("Rossz a megadott sz�m, pr�b�lja �jra!");
                        a = Convert.ToByte(Console.ReadLine());
                    }
                    ellenorzes = true;
                }
                catch (Exception) { Console.WriteLine("Rossz a megadott sz�m, pr�b�lja �jra!"); }
            }
            while (ellenorzes == false);
            Console.Write("Adja meg a nev�t: ");
            nev = Console.ReadLine();
            while (nev == "")
            {
                Console.Write("Adja meg a nev�t: ");
                nev = Console.ReadLine();
            }
            if (a > 5 && a < 9)
                Console.WriteLine("J� reggelt {0}!", nev);
            if (a > 8 && a < 19)
                Console.WriteLine("J� napot {0}!", nev);
            if (a > 18 && a < 23)
                Console.WriteLine("J� est�t {0}!", nev);
            if (a >= 0 && a <= 5 || a == 23)
                Console.WriteLine("J� �jszak�t {0}!", nev);
            Console.ReadLine();
            Console.Clear();
        }
        static void Hatodik() 
        {
            /*
            6. feladat:
            K�rj be a felhaszn�l�t�l �t sz�mot!
		    Mind az �t�t vizsg�ld meg, hogy p�ros-e vagy p�ratlan!
		    �rd ki a megfelel� �zenetet!
		    K�rj be a felhaszn�l�t�l egy sz�mot!
            Vizsg�ld meg, hogy a kor�bban bek�rt �t sz�m k�z�l melyik oszthat� marad�k n�lk�l ezzel a sz�mmal!
            */

            Console.Clear();
            int a = 0, b = 0, c = 0, d = 0, e = 0, be = 0;
            szambekeres(ref a);
            szambekeres(ref b);
            szambekeres(ref c);
            szambekeres(ref d);
            szambekeres(ref e);
            if (a % 2 == 0)
                Console.WriteLine("\n1: p�ros");
            else
                Console.WriteLine("\n1: p�ratlan");
            if (b % 2 == 0)
                Console.WriteLine("2: p�ros");
            else
                Console.WriteLine("2: p�ratlan");
            if (c % 2 == 0)
                Console.WriteLine("3: p�ros");
            else
                Console.WriteLine("3: p�ratlan");
            if (d % 2 == 0)
                Console.WriteLine("4: p�ros");
            else
                Console.WriteLine("4: p�ratlan");
            if (e % 2 == 0)
                Console.WriteLine("5: p�ros\n");
            else
                Console.WriteLine("5: p�ratlan\n");
            szambekeres(ref be);
            Console.WriteLine("\nA k�vetkez� sz�mok oszthat�ak marad�k n�lk�l a megadott sz�mmal:");
            if (a % be == 0)
                Console.WriteLine(a);
            if (b % be == 0)
                Console.WriteLine(b);
            if (c % be == 0)
                Console.WriteLine(c);
            if (d % be == 0)
                Console.WriteLine(d);
            if (e % be == 0)
                Console.WriteLine(e);
            Console.ReadLine();
            Console.Clear();
        }
        static void Hetedik() 
        {
            /*
            7. feladat:	K�rj be a felhaszn�l�t�l h�rom sz�mot!
		    Mind a h�rmat vizsg�ld meg, hogy p�ros-e vagy p�ratlan!
		    �rd ki a megfelel� �zenetet!
		    K�rj be a felhaszn�l�t�l k�t sz�mot!
            Vizsg�ld meg, hogy a kor�bban bek�rt h�rom sz�m k�z�l melyik oszthat� marad�k n�lk�l ezekkel a sz�mokkal!
            A ki�rhat� �zenetek a k�vetkez�k legyenek:
            �Az x. sz�m oszthat� mind a kett� sz�mmal!�
            �Az x. sz�m csak az els� sz�mmal oszthat�.�
            �Az x. sz�m csak a m�sodik sz�mmal oszthat�.�
            �Az x. sz�m egyik sz�mmal sem oszthat�.�
            */

            Console.Clear();
            int a = 0, b = 0, c = 0, d = 0, e = 0;

            szambekeres2(ref a, "els�", "sz�mot");
            szambekeres2(ref b, "m�sodik", "sz�mot");
            szambekeres2(ref c, "harmadik", "sz�mot");

            pp(a);
            pp(b);
            pp(c);

            szambekeres2(ref d, "els�", "oszt�t");
            szambekeres2(ref e, "m�sodik", "oszt�t");

            oszthatosag(a, d, e, "els�");
            oszthatosag(b, d, e, "m�sodik");
            oszthatosag(c, d, e, "harmadik");
            Console.ReadLine();
            Console.Clear();
        }
//ezek m�r nem men�pontok, hanem a program r�vid�t�se c�lj�b�l l�trehozott extra dolgok.
        static int szambekeres (ref int a) 
        {
            do
            {
                try
                {
                    Console.Write("Adjon meg egy sz�mot: ");
                    a = Convert.ToInt32(Console.ReadLine());
                    ellenorzes = true;
                }
                catch (Exception) { Console.WriteLine("Rossz a megadott sz�m, pr�b�lja �jra!"); }
            }
            while (ellenorzes == false); ellenorzes = false;
            return a;
        }
        static int szambekeres2(ref int a, string sorszam, string mit) 
        {
            do
            {
                try
                {
                    Console.Write("K�rem a(z) {0} {1}: ", sorszam, mit);
                    a = Convert.ToInt32(Console.ReadLine());
                    ellenorzes = true;
                }
                catch (Exception) { Console.WriteLine("Rossz a megadott sz�m, pr�b�lja �jra!"); }
            }
            while (ellenorzes == false); ellenorzes = false;
            return a;
        }
        static double valosbekeres(ref double a) 
        {
            do
            {
                try
                {
                    Console.WriteLine("Adjon meg az els� (val�s) sz�mot! (tizedespont = vessz�)");
                    a = Convert.ToDouble(Console.ReadLine());
                    ellenorzes = true;
                }
                catch (Exception) { Console.WriteLine("Rossz a megadott sz�m, pr�b�lja �jra!"); }
            }
            while (ellenorzes == false); ellenorzes = false;
            return a;
        }
        static void pp(int a) 
        {
            if (a % 2 == 0)
                Console.WriteLine("{0} p�ros", a);
            else
                Console.WriteLine("{0} p�ratlan", a);
        }
        static void oszthatosag (int szam, int elsooszto, int masodikoszto, string sorszam) 
        {
            if (szam % elsooszto == 0 || szam % masodikoszto == 0)
            {
                if (szam % elsooszto == 0 && szam % masodikoszto == 0)
                    Console.WriteLine("Az {0} sz�m oszthat� mindk�t oszt�val!", sorszam);
                else
                {
                    if (szam % elsooszto == 0)
                        Console.WriteLine("Az {0} sz�m csak az els� oszt�val oszthat�!", sorszam);
                    else
                        Console.WriteLine("Az {0} sz�m csak a m�sodik oszt�val oszthat�!", sorszam);
                }
            }
            else
                Console.WriteLine("Az 1. sz�m egyik sz�mmal sem oszthat�.");
        }
    }
}