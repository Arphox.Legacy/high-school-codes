using System;
using System.IO;

class Program
{
    static void Main()
    {
        do
        {
            Menu();
        } while (true);
    }
    static void Menu()
    {
        Console.Clear();
        Console.WriteLine(".:N�vjegyz�k:.\nV�lasszon men�pontot:\n1 - Keres�s\n2 - �j adat be�r�sa");
        Console.WriteLine("3 - Adatb�zis list�z�sa\n4 - Adatb�zis t�rl�se\n0 - Kil�p�s");
        string a = Convert.ToString(Console.ReadLine());
        switch (a)
        {
            case "1":
                Kereses();
                break;
            case "2":
                Ujadat();
                break;
            case "3":
                Listazas();
                break;
            case "4":
                Torles();
                break;
            case "0":
                Environment.Exit(-1);
                break;
            default:
                break;
        }
    }
    static void Kereses() 
    {
        Console.Clear();
        Console.WriteLine("1 - Keres�s");
        try //A try-catch az�rt kell, hogy ha nincs adatb�zis, akkor nincs mit olvasni.
        {
            StreamReader sr = new StreamReader("N�vjegyz�k.txt"); //egy olvas�sfolyam l�trehoz�sa a f�jlra
            string nev = "", sor;
            int i = 0;
            Console.Clear();
            Console.WriteLine("Adja meg keres�sz�t: ");
            nev = Console.ReadLine();
            Console.WriteLine("\nTal�latok:");
            while ((sor = sr.ReadLine()) != null) //Addig fut, am�g a v�g�re nem �r a f�jlnak.
            {
                /*
                * Az IndexOf() f�ggv�ny -1 �rt�ket ad vissza, ha a param�ter list�j�ban megadott karakter,
                * vagy karaktersorozat nem szerepel a string- ben. 0,vagy pozit�v �rt�ket, ha szerepel.
                */
                if (sor.IndexOf(nev) > -1)
                {
                    Console.WriteLine(sor);
                    i++;
                }
            }
            if (i == 0)
                Console.WriteLine("Nem tal�ltam a keres�snek megfelel� n�vjegyet.");
        }
        catch (Exception)
        {
            Console.WriteLine("Az adatb�zis �res, �rjon bele adatot, hogy kereshessen benne!");
        }
        Console.ReadLine();
    }
    static void Ujadat() 
    {
        string nev = "", email = "", valasz = "n";
        Console.Clear();
        Console.WriteLine("2 - �j adat be�r�sa");
        //Adatbe�r�s
        while (valasz == "n") //ha rosszul �rta be, megadhatja �jra az adatokat
        {
            Console.Clear();
            Console.WriteLine("2 - �j adat be�r�sa");
            Console.Write("Adja meg a nevet: ");
            nev = Console.ReadLine();
            Console.Write("Adja meg az e-mail c�met: ");
            email = Console.ReadLine();
            Console.WriteLine("\nN�v: " + nev);
            Console.WriteLine("E-mail: " + email);
            Console.WriteLine("Helyesen adta meg az adatokat, menthetem? i/n");
            valasz = Console.ReadLine();
            while (valasz != "n" && valasz != "i") //Csak i/n v�laszt fogadjon el
            {
                Console.WriteLine("Rossz a v�lasz");
                Console.WriteLine("Helyesen adta meg az adatokat, menthetem? i/n");
                valasz = Console.ReadLine();
            }
        }
//Itt kezd�dik a f�jlba be�r�s
        FileStream fs = new FileStream("N�vjegyz�k.txt", FileMode.Append);
        StreamWriter sw = new StreamWriter(fs);
        sw.WriteLine(nev+"\t"+email); //beker�l a f�jlba: [n�v]<tab>[c�m]<enter>
        sw.Close();
        fs.Close();
//F�jl�r�s v�ge
        Console.WriteLine("\nF�jl�r�s k�sz, nyomjon entert a visszal�p�shez.");
        Console.ReadLine();
    }
    static void Listazas() 
    {
        Console.Clear();
        Console.WriteLine("4 - Adatb�zis list�z�sa\n");
        try //A try-catch az�rt kell, hogy ha nincs adatb�zis, akkor nincs mit olvasni.
        {
            StreamReader sr = new StreamReader("N�vjegyz�k.txt");
            string sor = "";
            while ((sor = sr.ReadLine()) != null) //Addig fut, am�g a v�g�re nem �r a f�jlnak.
            {
                Console.WriteLine(sor);
            }
            Console.WriteLine("\nNyomjon entert a visszal�p�shez");
        }
        catch (Exception)
        {
            Console.WriteLine("Az adatb�zis �res, �rjon bele adatot, hogy kilist�zhassa!");
        }
        Console.ReadLine();
    }
    static void Torles() 
    {
        string valasz = "n";
        Console.Clear();
        Console.WriteLine("4 - Adatb�zis t�rl�se");
        Console.WriteLine("Biztos abban, hogy szeretn� a teljes adatb�zist t�r�lni? i/n");
        valasz = Console.ReadLine();
        while (valasz != "n" && valasz != "i") //Csak i/n v�laszt fogadjon el
        {
            Console.WriteLine("Rossz a v�lasz");
            Console.WriteLine("Biztos abban, hogy szeretn� a teljes adatb�zist t�r�lni? i/n");
            valasz = Console.ReadLine();
        }
        if (valasz == "n")
            goto skip;
        File.Delete("N�vjegyz�k" + ".txt");
        Console.WriteLine("Adatb�zis t�r�lve.");
        Console.ReadLine();
    skip: ;
    }
}